PREFIX=/usr/

all:
	# "You can now run make install"

install:
	install -m755 wifiberlin-autologin /usr/bin/wifiberlin-autologin
	install -m644 wifiberlin-autologin.service /lib/systemd/system/wifiberlin-autologin.service

uninstall:
	rm /usr/bin/wifiberlin-autologin /lib/systemd/system/wifiberlin-autologin.service
